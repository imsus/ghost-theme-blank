module.exports = (options, req) => ({
  entry: 'resources/index.js',
  dist: 'assets',
  html: 'index.html',
  filename: {
    js: 'js/[id].[name].js',
    css: 'css/[id].[name].css',
    static: 'static/[name].[ext]',
    chunk: 'js/[id].chunk.js'
  },
  presets: [
    require('poi-preset-buble')(),
    require('poi-preset-offline')({
      pluginOptions: {
        publicPath: '/assets/',
        caches: {
          main: ['css/*.css', 'js/*.js'],
          additional: [':externals:'],
          optional: [':rest:']
        },
        safeToUseOptionalCaches: true,
        ServiceWorker: {
          publicPath: '/assets/sw.js',
          minify: false
        },
        AppCache: false
      }
    })
  ]
})
